<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Author, Book};

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
        ]);

        Author::factory()->count(15)->create()->each(function($author) {
            // каждому автору создаем от нуля до четырех книг
            Book::factory()->count(rand(0, 4))->for($author)->create()->each(function($book) {
                // каждой книге добавляем от нуля до трех соавторов
                Author::where('id', '!=', $book->author_id)->inRandomOrder()->limit(rand(0, 3))->get()->each(function($author) use ($book) {
                    $book->coAuthors()->attach($author->id);
                });
            });
        });

    }
}
