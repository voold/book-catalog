<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Антон',
            'email' => 'admin@test.com',
            'email_verified_at' => now(),
            'password' => Hash::make(123654),
            'created_at' => now(),
        ]);
    }
}
