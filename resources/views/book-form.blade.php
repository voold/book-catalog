@extends('layouts/book-app')
@section('title', $title)

@section('content')
<div class="mt-5 md:mt-0 md:col-span-2">
  <div id="saved-alert" class="bg-green-100 border-l-4 border-green-500 text-green-700 m-1 p-3 hidden" role="alert">
    <p class="font-bold">Saved successfully.</p>
  </div>
  <div id="error-alert" class="bg-red-100 border-l-4 border-red-500 text-red-700 m-1 p-3 hidden" role="alert"></div>
  <form action="#" method="POST" id="form-edit">
    @csrf
    <div class="shadow sm:rounded-md sm:overflow-hidden">
      <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
        <div class="grid grid-cols-3 gap-6">
          <div class="col-span-3 sm:col-span-2">
            <label for="company-website" class="block text-sm font-medium text-gray-700">
              Title
            </label>
            <div class="mt-1 flex rounded-md shadow-sm">
                <input type="text" name="title" id="title" value="{{ $book->title ?? '' }}" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
            </div>
          </div>
        </div>

        <div>
          <label for="about" class="block text-sm font-medium text-gray-700">
            Description
          </label>
          <div class="mt-1">
            <textarea id="description" name="description" rows="3" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md">{{ $book->description ?? '' }}</textarea>
          </div>
        </div>

        <div>
          <label class="block text-sm font-medium text-gray-700">
            Author
          </label>
          <div class="mt-1 flex items-center">
              <select id="author_id" name="author_id" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                @foreach($authors as $author)
                    <option value="{{ $author->id }}" @if(isset($book) and $book->author->id == $author->id) selected @endif>{{ $author->name }}</option>
                @endforeach
              </select>
          </div>
        </div>
        <div>
          <label class="block text-sm font-medium text-gray-700">
            Co-author
          </label>
          <div class="mt-1 flex items-center">
              <select id="co_authors" name="co_authors[]" class="form-multiselect block w-full mt-1" multiple>
                  @foreach($authors as $author)
                      <option value="{{ $author->id }}" @if(isset($book) and $book->coAuthors->contains($author->id)) selected @endif>{{ $author->name }}</option>
                  @endforeach
              </select>
          </div>
        </div>

        <div class="grid grid-cols-3 gap-6">
          <div class="col-span-3 sm:col-span-2">
            <label for="company-website" class="block text-sm font-medium text-gray-700">
              Date publication
            </label>
            <div class="mt-1 flex rounded-md shadow-sm">
                <input type="text" name="publication_date" id="publication_date" value="@if(isset($book)) {{  $book->publication_date->format('Y-m-d') }} @endif" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
            </div>
          </div>
        </div>

      <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
        <button type="button" id="formBtn" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
          Save
        </button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#formBtn').click(function() {
        $('#error-alert').html('').hide();

        var data = $('#form-edit').serialize();

        $.ajax({
            type: 'POST',
            url: '{{ $route }}',
            data: data,
            dataType: 'json'
        }).done(function(response) {
            $('#saved-alert').show().delay(1500).fadeOut();
        }).fail(function(response) {
            var errors = response.responseJSON.errors;

            for(var error in errors) {
                $('#error-alert').append('<p>' + errors[error] + '</p>');
            }

            $('#error-alert').show();
        });
    });
});
</script>
@endsection
