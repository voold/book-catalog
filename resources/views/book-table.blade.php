@extends('layouts/book-app')
@section('title', 'Book catalog')

@section('content')
<div class="text-right mr-10">
    <a href="{{ route('book.create') }}" class="text-blue-600">Add new book</a>
</div>

<livewire:book-table />
@endsection
