<div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
  <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
    <table class="min-w-full divide-y divide-gray-200">
      <thead class="bg-gray-50">
        <tr>
            @foreach([['title', 'Title', 'sortable'],
                      ['name', 'Authors', 'sortable'],
                      ['co_authors', 'Co-authors'],
                      ['publication_date', 'Publication date', 'sortable']] as $th)
                    <th class="px-6 pt-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider @if(isset($th[2])) sortable cursor-pointer @endif" @if(isset($th[2])) wire:click="changeOrder('{{ $th[0] }}')" @endif>{{ __($th[1]) }}</th>
            @endforeach
            @auth
                <th></th>
                <th></th>
            @endauth
        </tr>
        <tr>
          <th scope="col" class="px-6 pb-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              <input class="shadow appearance-none border rounded w-full py-0 px-0" type="text" wire:model.debounce.500ms="title">
          </th>
          <th scope="col" class="px-6 pb-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              <input class="shadow appearance-none border rounded w-full py-0 px-0" type="text" wire:model.debounce.500ms="name">
          </th>
          <th scope="col" class="px-6 pb-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              <input class="shadow appearance-none border rounded w-full py-0 px-0" type="text" wire:model.debounce.500ms="co_authors">
          </th>
          <th scope="col" class="px-6 pb-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              <input class="shadow appearance-none border rounded w-full py-0 px-0" type="text" wire:model.debounce.500ms="publication_date">
          </th>
          @auth
              <th></th>
              <th></th>
          @endauth
        </tr>
      </thead>
      <tbody class="bg-white divide-y divide-gray-200">
        @foreach($books as $book)
            <tr>
                <td class="px-6 py-4 whitespace-nowrap">
                    <div class="flex items-center">
                        <div class="text-sm font-medium text-gray-900">
                            {{ $book->title }}
                        </div>
                    </div>
                </td>
                <td class="px-6 py-4 whitespace-nowrap">
                    <div class="text-sm text-gray-900">{{ $book->name }}</div>
                </td>
                <td class="px-6 py-4 whitespace-nowrap">
                    @foreach($book->coAuthors as $coAuthor)
                        <div class="text-sm text-gray-900">{{ $coAuthor->name }}</div>
                    @endforeach
                </td>
                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                    <div class="text-sm text-gray-900" date="{{ $book->publication_date }}">{{ $book->publication_date->toFormattedDateString() }}</div>
                </td>
                @auth
                    <td class="px-3 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <a href="{{ route('book.edit', $book->id) }}" class="text-indigo-600 hover:text-indigo-900 edit-button">Edit</a>
                    </td>
                    <td class="px-3 py-4 pr-5 whitespace-nowrap text-right text-sm font-medium">
                        <a href="#" wire:click="delete({{ $book->id }})" class="text-red-600 hover:text-red-900">Delete</a>
                    </td>
                @endauth
            </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="pt-5 p-3">{{ $books->links() }}</div>
</div>
