<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @livewireStyles
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    </head>
    <body>
        <div class="p-6 grid place-content-center">
            @guest
                <div class="text-blue-800" style="margin: 0 0 auto auto;">
                    <a href="/login" class="text-sm">Login</a>
                </div>
            @endguest
            <h1 class="text-2xl pb-4">@yield('title')</h1>
            <div class="flex flex-col">
              <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                @yield('content')
              </div>
            </div>
        </div>
        @livewireScripts
    </body>
</html>
