<?php
namespace App\Livewire;

trait SearchableTable
{
    public $order = ['column' => '', 'order' => ''];
    protected $supported_operations = ['!', '<', '>'];

    public function searchable($model)
    {
        foreach($this->params as $param) {
            // значение поля поиска
            $value = $this->{$param['name']};
            // tcли поисковое выражение не передавалось - пропускаем
            if($value == null)
                continue;

            // получаем команду из запроса, если она там есть
            $first_symbol = substr($value, 0, 1);
            $command = false;
            if(in_array($first_symbol, $this->supported_operations))
            {
              $command = ($first_symbol == '!') ? '!=' : $first_symbol;

              // отрезаем команду
              $value = substr($value, 1);
            }

            // условия в зависимости от типа парамтра
            $column = $this->columnToUseInRequest($param['name']);
            $function = 'where';
            $operation = $command ? $command : '=';
            switch($param['type'])
            {
                case 'string':
                    $operation = ($command == '!=') ? 'not like' : 'like';
                    $value = "%{$value}%";
                    break;
                case 'int': break;
                case 'date_day':
                case 'date_month':
                case 'date_year':
                    // явно задаем таблицу, которой принадлежит столбец
                    $column = $model->getQuery()->from . '.created_at';
                    // ...->whereMonth / whereDay / whereYear
                    $function = 'where' . substr($param['type'], strpos($param['type'], '_') + 1);
                    break;
            }

            // если есть объединенный параметр из нескольких таблиц
            if(isset($param['tables'])) {
                foreach($param['tables'] as $key => $table) {
                  $temp_function = ($key === 0) ? $function : "or{$function}";
                  $temp_column = "{$table}.{$column}";

                  $model = $model->$temp_function($temp_column, $operation, $value);
                }
            }
            else
                $model = $model->$function($column, $operation, $value);
        }

        return $model;
    }

    /**
     * Подключает сортировку по столбцам.
     * $model Model
     * string $orderBy столбец начальной сортировки, необязательный параметр
     * string $sort ASC или DESC, по умолчанию DESC
    */
    public function sortable($model, $orderBy = null, $sort = 'DESC')
    {
        if(!empty($this->order['column'])) {
            $model = $model->orderBy($this->order['column'], $this->order['order']);
        }
        if(!is_null($orderBy)) {
            // если задана начальная сортировка
            $model = $model->orderBy($orderBy, $sort);
        }


        return $model;
    }

    /*
     * Меняем сортировку у столбца
    */
    public function changeOrder($column)
    {
        // проставляем сортировку
        $this->order['order'] = ($column !== $this->order['column'] OR $this->order['order'] == 'DESC') ? 'ASC' : 'DESC';
        // и столбец, по которому будем сортировать
        $this->order['column'] = $column;

        $this->render();
    }

    private function columnToUseInRequest($column)
    {
        // если в названии параметра есть __ - меняем его на точку, чтобы получилось таблица.столбец
        $column = str_replace('__', '.', $column);

        return $column;
    }
}
