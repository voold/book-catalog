<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Livewire\SearchableTable;
use App\Models\Book;
use Illuminate\Support\Facades\Auth;

class BookTable extends Component
{
    use WithPagination, SearchableTable;

    public $title,
           $name,
           $co_authors,
           $publication_date;

    protected $params = [
        ['name' => 'title', 'type' => 'string'],
        ['name' => 'name', 'type' => 'string'],
        ['name' => 'publication_date', 'type' => 'string']
    ];


    public function render()
    {
        $books = new Book::withAuthorName();

        // поиск по соавторам
        if(!empty($this->co_authors)) {
            $books = $books->searchByCoAuthors($this->co_authors);
        }

        // подключаем поиск по столбцам и сортировку
        $books = $this->searchable($books);
        $books = $this->sortable($books, 'title');

        return view('livewire.book-table', ['books' => $books->paginate(10)]);
    }

    public function delete($book_id)
    {
        if(Auth::check()) {
            $book = Book::findOrFail($book_id);
            $book->delete();
        }
        else
            abort(403);
    }
}
