<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
use App\Models\{Book, Author};

class BookController extends Controller
{
    public function index() {
        $books = Book::paginate(10);
        return view('book-table', ['books' => $books]);
    }

    public function edit(Book $book) {
        $authors = Author::get();

        return view('book-form', [
            'title' => 'Edit book',
            'route' => route('book.update', $book->id),
            'book' => $book,
            'authors' => $authors
        ]);
    }

    public function create() {
        $authors = Author::get();

        return view('book-form', [
            'title' => 'Add new book',
            'route' => route('book.store'),
            'authors' => $authors
        ]);
    }

    public function update(BookRequest $request, Book $book) {
        $book->title = $request->title;
        $book->description = $request->description;
        $book->author_id = $request->author_id;
        $book->publication_date = $request->publication_date;

        $book->coAuthors()->sync($request->co_authors);

        $book->save();

        return true;
    }

    public function store(BookRequest $request) {
        $book = new Book;
        $book->title = $request->title;
        $book->description = $request->description;
        $book->author_id = $request->author_id;
        $book->publication_date = $request->publication_date;

        $book->save();

        $book->coAuthors()->attach($request->co_authors);

        return true;
    }
}
