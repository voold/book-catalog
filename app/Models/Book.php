<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $dates = ['created_at', 'updated_at', 'publication_date'];

    public function author() {
        return $this->belongsTo(Author::class);
    }

    public function coAuthors() {
        return $this->belongsToMany(Author::class, 'author_book');
    }

    public function searchByCoAuthors(string $search_string) {
        return $this->whereHas('coAuthors', function($query) use ($search_string) {
                   $query->where('name', 'like', "%{$search_string}%");
               });
    }

    public function scopeWithAuthorName($query) {
        return $query->leftJoin('authors', 'author_id', '=', 'authors.id')->select('books.*', 'authors.name');
    }
}
